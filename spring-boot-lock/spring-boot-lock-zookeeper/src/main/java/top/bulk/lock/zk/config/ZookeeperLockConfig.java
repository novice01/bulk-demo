package top.bulk.lock.zk.config;

import lombok.Data;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * zk 相关配置
 *
 * @author 散装java
 * @date 2022-12-30
 */
@Configuration
@ConfigurationProperties(prefix = "bulk.zookeeper")
@Data
public class ZookeeperLockConfig {

    private String zkServers;

    private int sessionTimeout = 30000;

    private int connectionTimeout = 5000;

    private int baseSleepTimeMs = 1000;

    private int maxRetries = 3;

    /**
     * 链接 zk
     * 向容器中注入  CuratorFramework 方便后面使用
     *
     * @return curatorFramework
     */
    @Bean(initMethod = "start", destroyMethod = "close")
    public CuratorFramework curatorFramework() {
        //  重试策略 （1000毫秒试1次，最多试3次）
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(this.baseSleepTimeMs, this.maxRetries);

        return CuratorFrameworkFactory.builder()
                .connectString(this.zkServers)
                .sessionTimeoutMs(this.sessionTimeout)
                .connectionTimeoutMs(this.connectionTimeout)
                .retryPolicy(retryPolicy)
                .build();
    }
}
