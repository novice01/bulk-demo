# Spring Boot 集成 zookeeper 分布式锁

这里以三台 zookeeper 集群的形式，来演示分布式锁

## zk 实现分布式锁的原理

> Zookeeper节点路径不能重复 保证唯一性。 临时节点+事件通知
> 
> Zookeeper 实现分布式锁具有天然的优势，临时顺序节点，可以有效的避免死锁问题，让客户端断开，那么就会删除当前临时节点，让下一个节点进行工作。

1. 所有请求进来，在/lock下创建 临时顺序节点 ，放心，zookeeper会帮你编号排序

2. 判断自己是不是/lock下最小的节点
    1. 是，获得锁（创建节点）
    2. 否，对前面小我一级的节点进行监听
3. 获得锁请求，处理完业务逻辑，释放锁（删除节点），后一个节点得到通知（比你年轻的死了，你
成为最嫩的了）
4. 重复步骤2

## 集成步骤

### 1. 首先引入相关依赖
这里使用 curator-recipes 来进行 zk 的相关操作
```xml
<!-- zk 分布式锁支持 -->
<dependency>
    <groupId>org.apache.curator</groupId>
    <artifactId>curator-recipes</artifactId>
    <version>5.1.0</version>
</dependency>
<!-- 数据库支持-->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.27</version>
</dependency>
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.5.2</version>
</dependency>

```

### 2. 配置 zk 


## 启动说明
如果是 docker 做的 zk 集群；需要在启动项目的电脑本地 hosts 中配置zk host name的集群映射关系，不然会出现空指针异常